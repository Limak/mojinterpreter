#include "Pair.h"
#include <string.h>


Pair::Pair(const Pair& right)
{
	this->someString = _strdup(right.someString);
	this->isNull = right.isNull;
	this->value = right.value;
}


Pair::Pair(char * string, int value)
{
	this->someString = string;
	this->value = value;
	this->isNull = true;
}

void Pair::setValue(int value)
{
	this->value = value;
}

void Pair::setisNull(bool isNull)
{
	this->isNull = isNull;
}

Pair::~Pair()
{
}
