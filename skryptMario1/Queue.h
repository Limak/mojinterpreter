#pragma once
#include <cstdio>
#include <cstring>
#include "constValues.h"

class Queue
{
public:
	Queue* first;
	Queue* last;
	char* value;
	Queue* next;
	

	Queue* find(char* value);
	void add(char * value);
	int size();
	void removeElement();
	char * getFirstElement();
	void printAll();
	bool isEmpty();
	Queue(Queue * queue, char * value);
	Queue();
	~Queue();
};

