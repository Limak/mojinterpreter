#pragma once
#define HASH_START_TAB_SIZE 1000
#define MAX_SIZE_TAB 1000
#define BASE_TO_HASH 53	//a=1, ca�a alfabet ma 52 litery (wlacznie z duzymi literami)
//do hashowania potrzebna jest koniecznie liczba pierwsza
#define MAX_LICZBA_ZMIENNYCH 1000	//najwieksza liczba zmiennych
#define MAX_LICZBA_KODU 1000
#define MAX_ROZMIAR_NAPISU 1000
#define DIFF_BETWEEN_SIZES 26

#define MAX_DL_NAPISU 256
#define MAX_DL_LINII 1000
#define ILOSC_OPERATOROW 16
