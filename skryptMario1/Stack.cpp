#define _CRT_SECURE_NO_WARNINGS
#include "Stack.h"
#include <string.h>



Stack::Stack()
{
	value[0] = '\0';
	next = NULL;
}

void Stack::showStack(Stack * stack)
{
	stack = stack->next;

	while (stack != NULL)
	{
		printf("%s ", stack->value);
		stack = stack->next;
	}
	printf("\n");
}

bool Stack::isEmpty()
{
	if (next == NULL)
		return true;
	return false;
}

void Stack::push(char* value)
{
	Stack* element = new Stack;
	//element->value = new char[FILENAME_MAX];

	//element->value = value;
	strcpy(element->value, value);
	element->next = next;
	next = element;

}

void Stack::pop()
{
	if (next != NULL)
		next = next->next;
}


int Stack::stackSize(Stack * stack)
{
	int counter = 0;

	stack = stack->next;
	if (stack == NULL)
		return 0;

	while (stack != NULL)
	{
		counter++;
		stack = stack->next;
	}
	return counter;
}

char* Stack::top()
{
	if (next != NULL)
		return next->value;
	return NULL;
}

void Stack::printAll()
{
	Stack* tmp = this;
	while(tmp != nullptr)
	{
		printf("%s ", tmp->value);
		tmp = tmp->next;
	}
	printf("\n");
}

Stack::~Stack()
{
}
