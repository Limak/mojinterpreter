#pragma once
#include "MapHash.h"
#include "Stack.h"

void wypelnijTabliceOperatorow();
int znajdzPriorytet(char* szukanyOperator);
void usunKoniecLinii(char* napis);
void wyswietlKod(char** tab, int dl);
void alokujPamiecDlaTablicyNapisow(char** napis, int iloscNapisow);
bool czyLitera(char znak);
bool czyOperator(char znak);
bool czyCyfra(char znak);
bool czyLiczba(char* napis);
char** szukajWyrazow(char* liniaKodu, int* iloscWyrazowLinii);
bool czyUnikatowaZmienna(char* zmiennaKandydat, char** wszystkieZmienneProgramu,
	int licznikWszystkichZmiennych);
char* znajdzWyraz(char* wczytanyKod, int* kolumnaKodu);
char* znajdzLiczbe(char* wczytanyKod, int* kolumnaKodu);
char pokazNajblizszyZnak(char* wczytanyKod, int kolumnaKodu);
char pokazNajblizszyZnak(char* wczytanyKod, int* kolumnaKodu);
bool czyZnakLiczbyUjemnej(char* wczytanyKod, int kolumnaKodu);
char* znajdzOperator(char* wczytanyKod, int* kolumnaKodu);
void wykonajPrzypisanieDoZmiennej(char* lewyOperand, char* prawyOperand,
	char* operacja, MapHash* zmienneProgramu, Stack* stosOperandow);
void wykonajDodawanie(char* lewyOperand, char* prawyOperand,
	char* operacja, MapHash* zmienneProgramu, Stack* stosOperandow);
void wykonajOdejmowanie(char* lewyOperand, char* prawyOperand,
	char* operacja, MapHash* zmienneProgramu, Stack* stosOperandow);
void wykonajMnozenie(char* lewyOperand, char* prawyOperand,
	char* operacja, MapHash* zmienneProgramu, Stack* stosOperandow);
void wykonajDzielenie(char* lewyOperand, char* prawyOperand,
	char* operacja, MapHash* zmienneProgramu, Stack* stosOperandow);
void znajdzRodzajOperacji(char* lewyOperand, char* prawyOperand, char* operacja,
	MapHash* zmienneProgramu, Stack* stosOperandow);
bool czyLiczycDalej(int* licznikOperacji, int maxOperacji);
void wykonajObliczeniaONP(Stack* operandy, Stack* operatoryZeStosu, MapHash* zmienneProgramu,
	int* licznikOperacji, int maxOperacji);
void zdejmijZeStosuPomiedzyNawiasami(Stack* stosOperatorow, Stack* stosOperandow, MapHash* zmienneProgramu,
	int* licznikOperacji, int maxOperacji);
void zbadajOperatoryDoKonwersji(char* znalezionyOperator, Stack* stosOperatorow,
	Stack* stosOperandow, MapHash* zmienneProgramu, int* licznikOperacji, int maxOperacji);
void dokonczONP(Stack* stosOperatorow, Stack* stosOperandow, MapHash* zmienneProgramu,
	int* licznikOperacji, int maxOperacji);
void wypelnijTabliceOperatorow();
void ZwiekszTablice(char ** tablicaDoZwiekszenia, int* rozmiarTablicy);
void wykonajONP(char* rownanie, MapHash* zmienneProgramu, Stack* stosOperandow,
	int* licznikOperacji, int maxOperacji);
int przesunIndex(char* wczytanyKod, int indexOrginalny);
char* znajdzWarunek(char* wczytanyKod, int* kolumnaInstrukcji, MapHash* zmienneProgramu, int* licznikOperacji,
	int maxOperacji);
bool liczWarunek(char* kodWarunku, MapHash* zmienneProgramu, int* licznikOperacji, int maxOperacji);
bool czyPrzekazacDoONP(char* wczytanyKod, MapHash* zmienneProgramu, Stack* stosOperandow,
	char* rownanie, int* kolumna, int* dlugoscKodu, int* licznikOperacji, int maxOperacji);
void sprawdzKoniecInstrukcji(char* wczytanyKod, char* rownanie, int* kolumnaKodu,
	int* dlugoscRownania, MapHash* zmienneProgramu, Stack* stosOperandow, int* licznikOperacji, int maxOperacji);
void wykonajIf(char* wczytanyKod, int* kolumnaKodu, MapHash* zmienneProgramu,
	Stack* stosOperandow, int* licznikOperacji, int maxOperacji);
void wykonajWhile(char * wczytanyKod, int * kolumnaKodu, MapHash * zmienneProgramu,
	Stack* stosOperandow, int* licznikOperacji, int maxOperacji);
void szukajRowaniaDoONP(char* wczytanyKod, MapHash* zmienneProgramu, int* licznikOperacji, int maxOperacji);
void wykonujPomiedzyKlamrami(char* wczytanyKod, int* kolumnaKodu, MapHash* zmienneProgramu,
	int* indexInstrukcji, Stack* stosOperandow, int* licznikOperacji, int maxOperacji);
