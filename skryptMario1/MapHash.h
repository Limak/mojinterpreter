#pragma once
#include <cstdio>
#include "constValues.h"
#include "Pair.h"

//klasa wlasnej implementacji mapy hashowanej

class MapHash
{
	Pair** hashTable;
	int arraySize;
	int numberOfElements;

public:

	MapHash();
	MapHash(int initialSize);
	void addElement(int value, char* key);
	void addElement(Pair* pair);
	int findElement(char* key);
	int findHashedIndex(char * key);
	void declareValue(char * key, int value);
	bool getIsNull(char * variable);
	void setIsNull(char * key);
	void setIsNotNull(char * key);
	Pair * getHashTableElement(int index);

	~MapHash();

private:
	bool isFull();
	int hashKey(char* someString);
	void resizeArray();
	void deleteHashArray();
	unsigned long long int findHashKey(char* someString);
};

