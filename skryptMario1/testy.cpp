#include "testy.h"
#include "skryptMario1.h"
#include <cstdlib>


void testDodawaniaPozaFunkcja(Queue* kolejka)
{
	kolejka->add("pozaFunkcja");
}

void testKolejki()
{
	Queue kolejka = Queue();

	char* a = "a";
	char* b = "b";
	char* c = "c";
	kolejka.add(a);
	printf("%s %s\n", kolejka.first->value, kolejka.last->value);
	kolejka.add(b);
	printf("%s %s\n", kolejka.first->value, kolejka.last->value);
	kolejka.add(c);
	printf("%s %s\n", kolejka.first->value, kolejka.last->value);
	kolejka.removeElement();
	printf("%s %s\n", kolejka.first->value, kolejka.last->value);
	kolejka.add("d");
	printf("%s %s\n", kolejka.first->value, kolejka.last->value);
	kolejka.add("e");
	printf("%s %s\n", kolejka.first->value, kolejka.last->value);
	testDodawaniaPozaFunkcja(&kolejka);
	printf("%s %s\n", kolejka.first->value, kolejka.last->value);
	kolejka.add("cos");
	printf("%s %s\n", kolejka.first->value, kolejka.last->value);
}

void testKonwersjiONP()
{
	char** wczytanyKod = new char*[1];
	MapHash tabHash;
	Stack stosTestowy = Stack();
	Queue testoweOnp;
	int testowaIloscLinii = 1;
	wypelnijTabliceOperatorow();
	wczytanyKod[0] = _strdup("a=b+4");
	//konwertujNaONP(wczytanyKod, &tabHash, &stosTestowy, &testoweOnp, testowaIloscLinii);
}

void testHashowania()
{
	MapHash hashTable;

	char* napis = _strdup("tytyty");
	hashTable.addElement(21, napis);
	int indexZahashowany = hashTable.findElement(napis);
	printf("%d\n", hashTable.findElement(napis));
}

void testDodawaniaNaHashowanejTablicy()
{
	MapHash hashTable;
	char* zmienna = _strdup("zma");
	char* zmienna2 = _strdup("zmb");
	hashTable.addElement(0, zmienna);
	hashTable.addElement(0, zmienna2);
	hashTable.declareValue(zmienna, 2);
	int wartoscZmiennej = hashTable.findElement(zmienna);
	hashTable.declareValue(zmienna2, wartoscZmiennej + 2);

	printf("%d\n", hashTable.findElement(zmienna));
	printf("%d\n", hashTable.findElement(zmienna2));



}

void testListy()
{
	MyList lista;
	lista.showList();
	lista.addTop(_strdup("a"));
	lista.showList();
	lista.addTop(_strdup("b"));
	lista.showList();
	lista.addTop(_strdup("c"));
	lista.showList();
	lista.clearList();
	lista.showList();

}

#define ROZMIAR_TABLICY_TESTOWEJ 2
void testZwiekszaniaListy()
{
	int tabSize = ROZMIAR_TABLICY_TESTOWEJ;
	char** tablicaNapisow = new char*[tabSize];
	tablicaNapisow[0] = _strdup("a");
	tablicaNapisow[1] = _strdup("b");
	//ZwiekszTablice(tablicaNapisow, &tabSize);
	realloc(tablicaNapisow, tabSize * 2);
	tabSize *= 2;
	tablicaNapisow[2] = _strdup("c");
}

void testRozmiaruTablicy()
{
	char**tab = new char*[10];
	for (int i=0;i<10;i++)
	{
		tab[i] = new char[2];
	}

	//printf("%d", sizeof(tab));
}

