#pragma once
#include <cstdio>
#include "Queue.h"
#include "MapHash.h"
#include "Stack.h"
#include <string.h>
#include "MyList.h"

void testDodawaniaPozaFunkcja(Queue* kolejka);
void testKolejki();
void testKonwersjiONP();
void testHashowania();
void testDodawaniaNaHashowanejTablicy();
void testListy();
void testZwiekszaniaListy();
void testRozmiaruTablicy();