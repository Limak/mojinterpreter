#pragma once
#include <cstdio>
class MyList
{
public:
	MyList* first;
	MyList* last;
	char* value;
	MyList* next;

	MyList();
	MyList(MyList* next, char* value);
	bool isEmpty();
	void removeBegin();
	void clearList();
	void addTop(char * value);
	char* findElement(char* value);
	MyList* findNode(char * value);
	void setValue(char* value);
	char* getValue(char* value);
	int size();
	void showList();
	~MyList();
};

