#include "MyList.h"
#include <string.h>


MyList::MyList():MyList(NULL, NULL)
{
	this->first = NULL;
	this->last = NULL;
	this->next = NULL;
}

MyList::MyList(MyList* next, char* value) :next(next), value(value)
{
	this->first = NULL;
	this->last = NULL;
	this->next = NULL;
}

void MyList::removeBegin()
{
	MyList* newElement = first;

	if (first == last)
	{
		first = newElement->next;
	}
	else
	{
		first = newElement->next;
	}
}

void MyList::clearList()
{
	MyList* tmp = first;

	while(tmp != last)
	{
		MyList* nextElement = tmp->next;
		delete tmp;
		tmp = nextElement;
	}
	first = last = NULL;
}

void MyList::addTop(char * value)
{
	if (first == NULL)
	{
		first = last = new MyList(NULL, value);
	}
	else
	{
		MyList* newElement = new MyList(NULL, value);
		if (first->next == nullptr)
			first->next = newElement;
		last->next = newElement;
		last = newElement;
	}
}

bool MyList::isEmpty()
{
	if (first == NULL)
		return true;
	return false;
}

char * MyList::findElement(char * value)
{
	if (first == NULL)
		return NULL;

	MyList* tmp = first;

	while (strcmp(tmp->value, value) != 0)
	{
		tmp = tmp->next;
	}

	if (strcmp(tmp->value, value) == 0)
	{
		return tmp->value;
	}
	else
		return NULL;
}

MyList* MyList::findNode(char* value)
{
	if (first == NULL)
		return NULL;

	MyList* tmp = first;

	while (strcmp(tmp->value, value) != 0)
	{
		tmp = tmp->next;
	}

	if (strcmp(tmp->value, value) == 0)
	{
		return tmp;
	}
	else
		return NULL;
}

int MyList::size()
{
	if (first == NULL)
		return 0;

	int size = 1;

	MyList* tmp = first;
	while (tmp->next != last)
	{
		tmp = tmp->next;
		size++;
	}
	delete tmp;
	return size;
}

void MyList::showList()
{
	MyList* tmp = first;
	if (first != NULL)
	{
		printf("%s ", tmp->value);
		while (tmp->next != NULL)
		{
			printf("%s ", tmp->next->value);
			tmp = tmp->next;
		}
	}
	printf("\n");
}


MyList::~MyList()
{
}
