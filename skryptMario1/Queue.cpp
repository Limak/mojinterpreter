#include "Queue.h"


Queue* Queue::find(char * value)
{
	if (first == NULL)
		return NULL;

	Queue* tmp = first;

	while (strcmp(tmp->value, value) != 0)
	{
		tmp = tmp->next;
	}

	if (strcmp(tmp->value, value) == 0)
	{
		return tmp;
	}
	else
		return NULL;
}

void Queue::add(char * value)
{

	if (first == NULL)
	{
		first = last = new Queue(NULL, value);
	}
	else
	{
		Queue* newElement = new Queue(NULL, value);
		if (first->next == nullptr)
			first->next = newElement;
		last->next = newElement;
		last = newElement;
	}

	// printf("%s %s\n", tmpLista.front(), tmpLista.back());

}

int Queue::size()
{
	if (first == NULL)
		return 0;

	int size = 1;

	Queue* tmp = first;
	while (tmp->next != last)
	{
		tmp = tmp->next;
		size++;
	}
	delete tmp;
	return size;
}

void Queue::removeElement()
{
	Queue* newElement = first;

	if (first == last)
	{
		first = newElement->next;
	}
	else
	{
		first = newElement->next;
	}
	
}

char* Queue::getFirstElement()
{
	return first->value;
}

void Queue::printAll()
{
	Queue* tmp = first;
	if(first != NULL)
	{
		printf("%s ", tmp->value);	
		while (tmp->next != NULL)
		{
			printf("%s ", tmp->next->value);
			tmp = tmp->next;
		}
	}
	printf("\n");
}

bool Queue::isEmpty()
{
	if (first == NULL)
		return true;
	return false;
}

Queue::Queue(Queue* queue, char* value) : next(queue), value(value)
{
	this->first = NULL;
	this->last = NULL;
	this->next = NULL;
}

Queue::Queue() : Queue(NULL, NULL)
{
}


Queue::~Queue()
{
}
