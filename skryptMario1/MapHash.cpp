#include "MapHash.h"
#include <string.h>
#include "constValues.h"



MapHash::MapHash()
{
	this->arraySize = HASH_START_TAB_SIZE;
	this->hashTable = new Pair*[arraySize];
	numberOfElements = 0;

	for (int i = 0; i < arraySize; i++)
	{
		hashTable[i] = NULL;
	}
}

MapHash::MapHash(int initialSize)
{
	this->arraySize = initialSize;
	this->hashTable = new Pair*[arraySize];
	numberOfElements = 0;

	for (int i = 0; i < arraySize; i++)
	{
		hashTable[i] = NULL;
	}
}

int MapHash::hashKey(char * someString)
{
	unsigned long long int key = findHashKey(someString);
	return key % this->arraySize;
}


void MapHash::addElement(int value, char* key)
{
	addElement(new Pair(key, value));
}

void MapHash::addElement(Pair* pair)
{
	numberOfElements++;
	int hash = hashKey(pair->someString);

	if (!isFull())
	{
		while (this->hashTable[hash] != NULL)
		{
			hash = hash + 1 % this->arraySize;
		}
	}
	else
	{
		resizeArray();

		while (this->hashTable[hash] != NULL)
		{
			hash = hash + 1 % this->arraySize;
		}
	}

	hashTable[hash] = pair;
}

int MapHash::findElement(char * key)
{
	int hash = hashKey(key);

	while (hashTable[hash] != NULL && strcmp(hashTable[hash]->someString, key) != 0)
	{
		hash = (hashKey(key) + 1) % this->arraySize;
	}

	if (hashTable[hash] == NULL)
	{
		return -1;
	}

	return hashTable[hash]->value;
}

int MapHash::findHashedIndex(char* key)
{
	int hash = hashKey(key);

	while (hashTable[hash] != NULL && strcmp(hashTable[hash]->someString, key) != 0)
	{
		hash = (hashKey(key) + 1) % this->arraySize;
	}

	if (hashTable[hash] == NULL)
	{
		return -1;
	}
	return hash;
}

void MapHash::declareValue(char* key, int value)
{
	int index = findHashedIndex(key);
	if (index != -1)
	{
		hashTable[index]->setValue(value);
		hashTable[index]->setisNull(false);
	}
	else
		return;
}

bool MapHash::getIsNull(char* key)
{
	int hashIndex = findHashedIndex(_strdup(key));

	if (hashIndex == -1)
		return true;
	return hashTable[hashIndex]->isNull;
}
void MapHash::setIsNull(char* key)
{
	int hashIndex = findHashedIndex(_strdup(key));
	hashTable[hashIndex]->setisNull(true);
}

void MapHash::setIsNotNull(char* key)
{
	int hashIndex = findHashedIndex(_strdup(key));
	hashTable[hashIndex]->setisNull(false);
}

Pair * MapHash::getHashTableElement(int index)
{
	return hashTable[index];
}

MapHash::~MapHash()
{
	deleteHashArray();
}

bool MapHash::isFull()
{
	return numberOfElements == arraySize;
}

void MapHash::resizeArray()
{
	MapHash newHashMap(arraySize * 2);
	for (int i = 0; i < arraySize; i++)
		if (hashTable[i] != nullptr)
		{
			Pair* copy = new Pair(*hashTable[i]);
			newHashMap.addElement(copy);
		}

	Pair** tempPair = newHashMap.hashTable;
	newHashMap.hashTable = this->hashTable;
	this->hashTable = tempPair;

	int tempSize = newHashMap.arraySize;
	newHashMap.arraySize = this->arraySize;
	this->arraySize = tempSize;

	int tempNumberOfElements = newHashMap.numberOfElements;
	newHashMap.numberOfElements = this->numberOfElements;
	this->numberOfElements = tempNumberOfElements;
}

void MapHash::deleteHashArray()
{
	for (int i = 0; i < this->arraySize; i++)
	{
		if (hashTable[i])
			delete hashTable[i];
	}
	delete[] hashTable;
}

unsigned long long int MapHash::findHashKey(char * someString)
{
	unsigned long long int result = 0;

	if (someString[0] >= 'a' && someString[0] <= 'z')
		result = (int)someString[0] - 'a' + DIFF_BETWEEN_SIZES + 1;	//wynik += 1 bo pierwsza litera "mojego systemu liczbowego" wynosi A
	else if (someString[0] >= 'A' && someString[0] <= 'Z')
		result += (int)(someString[0] - 'A' + 1);

	unsigned long long int wartoscPotegi = 1;


	bool isLengthEqualOne = someString[1] == '\0';

	if (isLengthEqualOne)
		return result;
	else
	{
		for (int i = 1; someString[i] != '\0'; i++)
		{
			wartoscPotegi *= BASE_TO_HASH;
			if (someString[i] >= 'a' && someString[i] <= 'z')
			{
				result += wartoscPotegi*(someString[i] - 'a' + DIFF_BETWEEN_SIZES + 1);
			}
			else if (someString[i] >= 'A' && someString[i] <= 'Z')
			{
				result += wartoscPotegi*(someString[i] - 'A' + 1);
			}

		}
		return result;
	}
}
