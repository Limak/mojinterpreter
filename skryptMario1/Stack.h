#pragma once
#include <cstdio>
#include "constValues.h"

class Stack
{
public:
	char value[MAX_DL_NAPISU];
	Stack* next;
	Stack();
	void showStack(Stack* stack);
	bool isEmpty();
	void push(char* value);
	void pop();
	int stackSize(Stack* stack);
	char* top();
	void printAll();
	~Stack();
};

