#pragma once
#define _CRT_SECURE_NOP_WARNINGS
#include <cstdio>
#include <cstring>
#include "constValues.h"
#include "MapHash.h"
#include "Stack.h"
#include "Pair.h"
//#include "testy.h"
#include <cctype>
#include "skryptMario1.h"
#include <cstdlib>


struct Operatory
{
	char* operatorString;
	int priorytet;
}tablicaOperatorow[16];

void wypelnijTabliceOperatorow()
{
	tablicaOperatorow[0].operatorString = _strdup("=");
	tablicaOperatorow[0].priorytet = 1;

	tablicaOperatorow[1].operatorString = _strdup("|");
	tablicaOperatorow[1].priorytet = 2;

	tablicaOperatorow[2].operatorString = _strdup("&");
	tablicaOperatorow[2].priorytet = 3;

	tablicaOperatorow[3].operatorString = _strdup("!=");
	tablicaOperatorow[3].priorytet = 4;

	tablicaOperatorow[4].operatorString = _strdup("==");
	tablicaOperatorow[4].priorytet = 4;

	tablicaOperatorow[5].operatorString = _strdup("<");
	tablicaOperatorow[5].priorytet = 5;

	tablicaOperatorow[6].operatorString = _strdup(">");
	tablicaOperatorow[6].priorytet = 5;

	tablicaOperatorow[7].operatorString = _strdup("<=");
	tablicaOperatorow[7].priorytet = 5;

	tablicaOperatorow[8].operatorString = _strdup(">=");
	tablicaOperatorow[8].priorytet = 5;

	tablicaOperatorow[9].operatorString = _strdup("+");
	tablicaOperatorow[9].priorytet = 6;

	tablicaOperatorow[10].operatorString = _strdup("-");
	tablicaOperatorow[10].priorytet = 6;

	tablicaOperatorow[11].operatorString = _strdup("/");
	tablicaOperatorow[11].priorytet = 7;

	tablicaOperatorow[12].operatorString = _strdup("*");
	tablicaOperatorow[12].priorytet = 7;

	tablicaOperatorow[13].operatorString = _strdup("%");
	tablicaOperatorow[13].priorytet = 7;

	tablicaOperatorow[14].operatorString = _strdup("!");
	tablicaOperatorow[14].priorytet = 8;

	tablicaOperatorow[15].operatorString = _strdup("-u");
	tablicaOperatorow[15].priorytet = 8;
}

int znajdzPriorytet(char* szukanyOperator)
{
	for (int i = 0; i < ILOSC_OPERATOROW; i++)
	{
		if (strcmp(szukanyOperator, tablicaOperatorow[i].operatorString) == 0)
			return tablicaOperatorow[i].priorytet;
	}
	return -1;
}

void usunKoniecLinii(char* napis)
{
	int dl = strlen(napis);

	napis[dl - 1] = ' ';
	return;
}

void wyswietlKod(char** tab, int dl)
{
	for (int i = 0; i < dl; i++)
	{
		for (int j = 0; j < strlen(tab[i]); j++)
		{
			printf("%c", tab[i][j]);
		}
	}
}


void alokujPamiecDlaTablicyNapisow(char** napis, int iloscNapisow)
{
	for (int i = 0; i < iloscNapisow; i++)
	{
		napis[i] = new char[MAX_DL_NAPISU];
	}
}

bool czyLitera(char znak)
{
	if (znak >= 'a' && znak <= 'z' || znak >= 'A' && znak <= 'Z')
		return true;
	return false;
}

bool czyOperator(char znak)
{
	if (znak == '+' || znak == '-' || znak == '*' || znak == '/' ||
		znak == '%' || znak == '<' || znak == '>' || znak == '!' ||
		znak == '&' || znak == '|' || znak == '!' || znak == '=')
		return true;
	return false;
}

bool czyCyfra(char znak)
{
	if (znak >= '0' && znak <= '9')
		return true;
	return false;
}

bool czyLiczba(char* napis)
{
	int dlugosc = strlen(napis);
	if (isdigit(napis[0]) || (napis[0] == '-' && isdigit(napis[1])))
		return true;
	return false;
}

char** szukajWyrazow(char* liniaKodu, int* iloscWyrazowLinii)
{
	int dlugoscNapisu = strlen(liniaKodu);
	char* wyraz = new char[MAX_DL_NAPISU];
	char** wyrazy = new char*[MAX_DL_LINII];
	int iloscWyrazow = 0;
	int indexWyrazu = 0;
	bool czyCiagLiter = true;

	alokujPamiecDlaTablicyNapisow(wyrazy, MAX_DL_LINII);

	for (int i = 0; i < dlugoscNapisu; i++)
	{
		if ((isalpha(liniaKodu[i]) && czyCiagLiter) ||
			(isalpha(liniaKodu[i]) && !czyCiagLiter))
		{
			wyraz[indexWyrazu] = liniaKodu[i];
			++indexWyrazu;
			czyCiagLiter = true;
		}
		else if (!isalpha(liniaKodu[i]) && czyCiagLiter)
		{
			wyraz[indexWyrazu] = '\0';
			indexWyrazu = 0;
			strcpy(wyrazy[iloscWyrazow], wyraz);
			wyraz[indexWyrazu] = '\0';
			++iloscWyrazow;
			czyCiagLiter = false;
		}
		else
		{
			indexWyrazu = 0;
			czyCiagLiter = false;
		}
	}

	if (czyCiagLiter)
	{
		wyraz[indexWyrazu] = '\0';
		indexWyrazu = 0;
		strcpy(wyrazy[iloscWyrazow], wyraz);
		wyraz[indexWyrazu] = '\0';
		++iloscWyrazow;
		czyCiagLiter = false;
	}


	*iloscWyrazowLinii = iloscWyrazow;
	delete[] wyraz;

	return wyrazy;
}

bool czyUnikatowaZmienna(char* zmiennaKandydat, char** wszystkieZmienneProgramu,
	int licznikWszystkichZmiennych)
{
	for (int i = 0; i < licznikWszystkichZmiennych; i++)
	{
		if (strcmp(zmiennaKandydat, wszystkieZmienneProgramu[i]) == 0)
			return false;
	}
	return true;
}

char* znajdzWyraz(char* wczytanyKod, int* kolumnaKodu)
{
	char* znalezionyWyraz = new char[MAX_DL_NAPISU];
	int dlugoscWyrazu = 0;
	while (isalpha(wczytanyKod[*kolumnaKodu]))
	{
		znalezionyWyraz[dlugoscWyrazu] = wczytanyKod[*kolumnaKodu];
		(*kolumnaKodu)++;
		dlugoscWyrazu++;
	}
	znalezionyWyraz[dlugoscWyrazu] = '\0';
	return znalezionyWyraz;
}

char* znajdzLiczbe(char* wczytanyKod, int* kolumnaKodu)
{
	char* znalezionaLiczba = new char[MAX_DL_NAPISU];
	int dlugoscLiczby = 0;

	if (wczytanyKod[*kolumnaKodu] == '-')
	{
		znalezionaLiczba[0] = '-';
		dlugoscLiczby++;
		(*kolumnaKodu)++;
	}
	while (czyCyfra(wczytanyKod[*kolumnaKodu]))
	{
		znalezionaLiczba[dlugoscLiczby] = wczytanyKod[*kolumnaKodu];
		(*kolumnaKodu)++;
		dlugoscLiczby++;
	}
	znalezionaLiczba[dlugoscLiczby] = '\0';
	return znalezionaLiczba;
}

char pokazNajblizszyZnak(char* wczytanyKod, int* kolumnaKodu)
{
	while (wczytanyKod[*kolumnaKodu] == ' ')
	{
		(*kolumnaKodu)++;
	}
	return wczytanyKod[*kolumnaKodu];
}

char pokazNajblizszyZnak(char* wczytanyKod, int kolumnaKodu)
{
	kolumnaKodu++;
	while (wczytanyKod[kolumnaKodu] == ' ' ||
		wczytanyKod[kolumnaKodu] == '\t' || wczytanyKod[kolumnaKodu] == '\n')
	{
		if (kolumnaKodu < strlen(wczytanyKod))
			kolumnaKodu++;
		else
			break;
	}
	return wczytanyKod[kolumnaKodu];
}

bool czyZnakLiczbyUjemnej(char* wczytanyKod, int kolumnaKodu)
{
	if (wczytanyKod[kolumnaKodu] == '=' && pokazNajblizszyZnak(wczytanyKod, kolumnaKodu) == '-')
		return true;
	return false;
}

char* znajdzOperator(char* wczytanyKod, int* kolumnaKodu)
{
	char*znalezionyOperator = new char[MAX_DL_NAPISU];
	int dlugoscOperatora = 0;
	while (czyOperator(wczytanyKod[*kolumnaKodu]) && !czyZnakLiczbyUjemnej(wczytanyKod, *kolumnaKodu))
	{
		znalezionyOperator[dlugoscOperatora] = wczytanyKod[*kolumnaKodu];
		(*kolumnaKodu)++;
		dlugoscOperatora++;
	}

	if (czyOperator(wczytanyKod[*kolumnaKodu]) && czyZnakLiczbyUjemnej(wczytanyKod, *kolumnaKodu))
	{
		znalezionyOperator[dlugoscOperatora] = wczytanyKod[*kolumnaKodu];
		(*kolumnaKodu)++;
		dlugoscOperatora++;
		znalezionyOperator[dlugoscOperatora] = '\0';
		return znalezionyOperator;
	}
	znalezionyOperator[dlugoscOperatora] = '\0';
	return znalezionyOperator;
}

char* formatujKod(char** wczytanyKod, int iloscLiniiKodu)
{
	int maxDlugoscKodu = MAX_DL_NAPISU;
	char* sformatowanyKod = new char[MAX_DL_LINII];
	int dlugoscKodu = 0;
	int i = 0;

	while (i < iloscLiniiKodu)
	{
		int j = 0;
		int dlugoscLinii = strlen(wczytanyKod[i]);

		while (j < dlugoscLinii)
		{
			if (dlugoscKodu == maxDlugoscKodu - 1)
			{
				maxDlugoscKodu *= 2;
				realloc(sformatowanyKod, maxDlugoscKodu*sizeof(char));
			}
			sformatowanyKod[dlugoscKodu] = wczytanyKod[i][j];
			++dlugoscKodu;
			++j;
		}
		++i;
	}
	sformatowanyKod[dlugoscKodu] = '\0';
	return sformatowanyKod;
}

void czyWartoscZmiennejNul(char* stronaOparenda, int* wartoscOperanda,
	MapHash* zmienneProgramu, Stack* stosOperandow)
{
	if (!zmienneProgramu->getIsNull(stronaOparenda))
		*wartoscOperanda = zmienneProgramu->findElement(stronaOparenda);
	else
	{
		char* nul = "#";
		//stosOperandow->push(_strdup(nul));
		stronaOparenda = _strdup(nul);
		return;
	}
	return;
}

void czyWartoscZmiennejNul(char* lewyOperand, char* prawyOperand,
	MapHash* zmienneProgramu, Stack* stosOperandow)
{
	char* nul = "#";
	if (zmienneProgramu->getIsNull(lewyOperand))
	{

		//stosOperandow->push(_strdup(nul));
		lewyOperand = _strdup(nul);
		return;
	}
	else if (zmienneProgramu->getIsNull(prawyOperand))
	{
		lewyOperand = _strdup(nul);
	}
	return;
}

void czyscStos(Stack* stos)
{
	while (!stos->isEmpty())
	{
		stos->pop();
	}
	return;
}


//jesli true to znak nulla wrzcanny jest na stos
bool czyKtorasStronaNul(Stack* stosOperandow, char* lewyOperand, char* prawyOperand)
{
	///GDY KTORYS OPERAND NUL '#'
	if (strcmp(lewyOperand, "#") == 0 || strcmp(prawyOperand, "#") == 0)
	{
		stosOperandow->push(_strdup("#"));
		return true;
	}
	return false;
}

void zarzadzajRodzajemOperandu(char* lewyOperand, char* prawyOperand,
	int* liczbaLewyOperand, int* liczbaPrawyOperand, MapHash* zmienneProgramu, Stack* stosOperandow)
{
	///GDY PRAWY OPERAND LICZBA
	if (!czyLiczba(lewyOperand) && czyLiczba(prawyOperand))
	{
		*liczbaPrawyOperand = atoi(prawyOperand);
		*liczbaLewyOperand = zmienneProgramu->findElement(lewyOperand);

	}
	///GDY LEWY OPERAND LICZBA
	else if (czyLiczba(lewyOperand) && !czyLiczba(prawyOperand))
	{
		*liczbaLewyOperand = atoi(lewyOperand);
		*liczbaPrawyOperand = zmienneProgramu->findElement(prawyOperand);

		czyWartoscZmiennejNul(prawyOperand, liczbaPrawyOperand, zmienneProgramu, stosOperandow);


	}
	///GDY OBA OPERANDY LICZBA
	else if (czyLiczba(lewyOperand) && czyLiczba(prawyOperand))
	{
		*liczbaLewyOperand = atoi(lewyOperand);
		*liczbaPrawyOperand = atoi(prawyOperand);
	}

	///GDY OBA OPERANDY ZMIENNE
	else
	{
		*liczbaLewyOperand = zmienneProgramu->findElement(lewyOperand);
		*liczbaPrawyOperand = zmienneProgramu->findElement(prawyOperand);

		czyWartoscZmiennejNul(lewyOperand, prawyOperand, zmienneProgramu, stosOperandow);
	}
}

void wykonajPrzypisanieDoZmiennej(char* lewyOperand, char* prawyOperand,
	char* operacja, MapHash* zmienneProgramu, Stack* stosOperandow)
{
	int liczbaWPrzypadkuWystapienia = 0;
	if (czyLiczba(prawyOperand))
	{
		liczbaWPrzypadkuWystapienia = atoi(prawyOperand);
	}

	if (strcmp(operacja, "=") == 0 && czyLiczba(prawyOperand))
		zmienneProgramu->declareValue(lewyOperand, liczbaWPrzypadkuWystapienia);
	else if (strcmp(operacja, "=") == 0 && !czyLiczba(prawyOperand) && strcmp(prawyOperand, "#"))
	{
		//JESLI JEST NULL TO POMIMO JAKIEJKOLWIEK WARTOSCI WYSWIETLI "Nul"
		if (!zmienneProgramu->getIsNull(prawyOperand))
		{
			int wartoscPrawegoOperandu = zmienneProgramu->findElement(prawyOperand);
			zmienneProgramu->declareValue(lewyOperand, wartoscPrawegoOperandu);
		}
		else
		{
			if (!zmienneProgramu->getIsNull(lewyOperand))
			{
				zmienneProgramu->setIsNull(lewyOperand);
				stosOperandow->push(_strdup("#"));
				return;
			}
		}
	}
	else if(strcmp(prawyOperand,"#")==0)
	{
		zmienneProgramu->setIsNull(lewyOperand);
		return;
	}

	//NA KONIEC WRZUCAM NA STOS
	stosOperandow->push(prawyOperand);
}

void wykonajDodawanie(char* lewyOperand, char* prawyOperand,
	char* operacja, MapHash* zmienneProgramu, Stack* stosOperandow)
{
	int liczbaPrawyOperand = 0;
	int liczbaLewyOperand = 0;
	char* wynikDodawaniaString = new char[MAX_DL_NAPISU];
	int wynikDodawaniaLiczb = 0;

	zarzadzajRodzajemOperandu(lewyOperand, prawyOperand, &liczbaLewyOperand,
		&liczbaPrawyOperand, zmienneProgramu, stosOperandow);

	if (czyKtorasStronaNul(stosOperandow, lewyOperand, prawyOperand))
		return;

	wynikDodawaniaLiczb = liczbaLewyOperand + liczbaPrawyOperand;

	itoa(wynikDodawaniaLiczb, wynikDodawaniaString, 10);
	stosOperandow->push(wynikDodawaniaString);
	delete[] wynikDodawaniaString;
	return;
}

void wykonajOdejmowanie(char* lewyOperand, char* prawyOperand,
	char* operacja, MapHash* zmienneProgramu, Stack* stosOperandow)
{
	int liczbaPrawyOperand = 0;
	int liczbaLewyOperand = 0;
	int wynikOdejmowania = 0;
	char* wynikOdejmowaniaString = new char[MAX_DL_NAPISU];

	zarzadzajRodzajemOperandu(lewyOperand, prawyOperand, &liczbaLewyOperand,
		&liczbaPrawyOperand, zmienneProgramu, stosOperandow);

	if (czyKtorasStronaNul(stosOperandow, lewyOperand, prawyOperand))
		return;

	wynikOdejmowania = liczbaLewyOperand - liczbaPrawyOperand;
	_itoa(wynikOdejmowania, wynikOdejmowaniaString, 10);
	stosOperandow->push(wynikOdejmowaniaString);
	delete[] wynikOdejmowaniaString;
	return;
}

void wykonajMnozenie(char* lewyOperand, char* prawyOperand,
	char* operacja, MapHash* zmienneProgramu, Stack* stosOperandow)
{
	int liczbaPrawyOperand = 0;
	int liczbaLewyOperand = 0;
	int wynikMnozenia = 0;
	char* wynikMnozeniaString = new char[MAX_DL_NAPISU];

	zarzadzajRodzajemOperandu(lewyOperand, prawyOperand, &liczbaLewyOperand,
		&liczbaPrawyOperand, zmienneProgramu, stosOperandow);

	if (czyKtorasStronaNul(stosOperandow, lewyOperand, prawyOperand))
		return;

	wynikMnozenia = liczbaLewyOperand * liczbaPrawyOperand;
	itoa(wynikMnozenia, wynikMnozeniaString, 10);
	stosOperandow->push(wynikMnozeniaString);

	delete[] wynikMnozeniaString;
}

void wykonajDzielenie(char* lewyOperand, char* prawyOperand,
	char* operacja, MapHash* zmienneProgramu, Stack* stosOperandow)
{
	int liczbaPrawyOperand = 0;
	int liczbaLewyOperand = 0;
	int wynikDzielenia = 0;
	char* wynikDzieleniaString = new char[MAX_DL_NAPISU];

	zarzadzajRodzajemOperandu(lewyOperand, prawyOperand, &liczbaLewyOperand,
		&liczbaPrawyOperand, zmienneProgramu, stosOperandow);

	if (czyKtorasStronaNul(stosOperandow, lewyOperand, prawyOperand))
		return;

	///GDY DZIELNIK JEST ZEREM WRZUCAM NULL NA STOS OPERANDOW
	if (liczbaPrawyOperand == 0)
	{
		char* nul = "#";	//znak nula na stosie
		stosOperandow->push(_strdup(nul));
		delete[] wynikDzieleniaString;
		return;
	}
	wynikDzielenia = liczbaLewyOperand / liczbaPrawyOperand;
	itoa(wynikDzielenia, wynikDzieleniaString, 10);
	stosOperandow->push(wynikDzieleniaString);
	delete[] wynikDzieleniaString;
	return;;
}

void wykonajModulo(char* lewyOperand, char* prawyOperand,
	char* operacja, MapHash* zmienneProgramu, Stack* stosOperandow)
{
	int liczbaPrawyOperand = 0;
	int liczbaLewyOperand = 0;
	int wynikModulo = 0;
	char* wynikModuloString = new char[MAX_DL_NAPISU];

	zarzadzajRodzajemOperandu(lewyOperand, prawyOperand, &liczbaLewyOperand,
		&liczbaPrawyOperand, zmienneProgramu, stosOperandow);

	if (czyKtorasStronaNul(stosOperandow, lewyOperand, prawyOperand))
		return;

	wynikModulo = liczbaLewyOperand % liczbaPrawyOperand;
	itoa(wynikModulo, wynikModuloString, 10);
	stosOperandow->push(wynikModuloString);
	delete[] wynikModuloString;
	return;
}

void wykonajPorownanie(char* lewyOperand, char* prawyOperand, char* operacja,
	MapHash* zmienneProgramu, Stack* stosOperandow)
{
	int liczbaLewyOperand = 0;
	int liczbaPrawyOperand = 0;
	char* wynikPorownaniaString = new char[MAX_DL_NAPISU];

	zarzadzajRodzajemOperandu(lewyOperand, prawyOperand, &liczbaLewyOperand,
		&liczbaPrawyOperand, zmienneProgramu, stosOperandow);

	if (czyKtorasStronaNul(stosOperandow, lewyOperand, prawyOperand))
		return;
	else if (liczbaLewyOperand == liczbaPrawyOperand)
		stosOperandow->push(_strdup("0"));
	else if (liczbaLewyOperand != liczbaPrawyOperand)
		stosOperandow->push(_strdup("#"));



	delete[] wynikPorownaniaString;
}

void wykonajWiekszy(char* lewyOperand, char* prawyOperand, char* operacja,
	MapHash* zmienneProgramu, Stack* stosOperandow)
{
	int liczbaLewyOperand = 0;
	int liczbaPrawyOperand = 0;
	char* wynikWiekszosciLewego = new char[MAX_DL_NAPISU];

	zarzadzajRodzajemOperandu(lewyOperand, prawyOperand, &liczbaLewyOperand,
		&liczbaPrawyOperand, zmienneProgramu, stosOperandow);


	if (czyKtorasStronaNul(stosOperandow, lewyOperand, prawyOperand))
		return;
	else if (liczbaLewyOperand > liczbaPrawyOperand)
		stosOperandow->push(_strdup("0"));
	else
		stosOperandow->push(_strdup("#"));

	delete[] wynikWiekszosciLewego;
}

void wykonajNierowny(char* lewyOperand, char* prawyOperand, char* operacja,
	MapHash* zmienneProgramu, Stack* stosOperandow)
{
	int liczbaLewyOperand = 0;
	int liczbaPrawyOperand = 0;
	char* wynikNierownosci = new char[MAX_DL_NAPISU];

	zarzadzajRodzajemOperandu(lewyOperand, prawyOperand, &liczbaLewyOperand,
		&liczbaPrawyOperand, zmienneProgramu, stosOperandow);


	if (czyKtorasStronaNul(stosOperandow, lewyOperand, prawyOperand))
		return;
	else if (liczbaLewyOperand != liczbaPrawyOperand)
		stosOperandow->push(_strdup("0"));
	else
		stosOperandow->push(_strdup("#"));

	delete[] wynikNierownosci;
}

void wykonajMniejszy(char* lewyOperand, char* prawyOperand, char* operacja,
	MapHash* zmienneProgramu, Stack* stosOperandow)
{
	int liczbaLewyOperand = 0;
	int liczbaPrawyOperand = 0;
	char* wynikMniejszosciLewego = new char[MAX_DL_NAPISU];

	zarzadzajRodzajemOperandu(lewyOperand, prawyOperand, &liczbaLewyOperand,
		&liczbaPrawyOperand, zmienneProgramu, stosOperandow);

	if (czyKtorasStronaNul(stosOperandow, lewyOperand, prawyOperand))
		return;
	else if (liczbaLewyOperand < liczbaPrawyOperand)
		stosOperandow->push(_strdup("0"));
	else
		stosOperandow->push(_strdup("#"));

	delete[] wynikMniejszosciLewego;
}

void wykonajMniejszyRowny(char* lewyOperand, char* prawyOperand, char* operacja,
	MapHash* zmienneProgramu, Stack* stosOperandow)
{
	int liczbaLewyOperand = 0;
	int liczbaPrawyOperand = 0;
	char* wynikNierownosci = new char[MAX_DL_NAPISU];

	zarzadzajRodzajemOperandu(lewyOperand, prawyOperand, &liczbaLewyOperand,
		&liczbaPrawyOperand, zmienneProgramu, stosOperandow);

	if (czyKtorasStronaNul(stosOperandow, lewyOperand, prawyOperand))
		return;
	else if (liczbaLewyOperand <= liczbaPrawyOperand)
		stosOperandow->push(_strdup("0"));
	else
		stosOperandow->push(_strdup("#"));
	delete[] wynikNierownosci;
}

void wykonajKoniunkcje(char* lewyOperand, char* prawyOperand, char* operacja,
	MapHash* zmienneProgramu, Stack* stosOperandow)
{
	int liczbaLewyOperand = 0;
	int liczbaPrawyOperand = 0;
	char* wynikKoniunkcji = new char[MAX_DL_NAPISU];

	zarzadzajRodzajemOperandu(lewyOperand, prawyOperand, &liczbaLewyOperand,
		&liczbaPrawyOperand, zmienneProgramu, stosOperandow);

	if (strcmp(lewyOperand, "0") && strcmp(prawyOperand, "0"))
		stosOperandow->push(_strdup("0"));
	else
		stosOperandow->push(_strdup("#"));


	delete[] wynikKoniunkcji;
}
void wykonajAlternatywe(char* lewyOperand, char* prawyOperand, char* operacja,
	MapHash* zmienneProgramu, Stack* stosOperandow)
{
	int liczbaLewyOperand = 0;
	int liczbaPrawyOperand = 0;
	char* wynikAlternatywy = new char[MAX_DL_NAPISU];

	zarzadzajRodzajemOperandu(lewyOperand, prawyOperand, &liczbaLewyOperand,
		&liczbaPrawyOperand, zmienneProgramu, stosOperandow);

	if (strcmp(lewyOperand, "#") && strcmp(prawyOperand, "#"))
		stosOperandow->push(_strdup("#"));
	else
		stosOperandow->push(_strdup("0"));


	delete[] wynikAlternatywy;
}

void wykonajWiekszyRowny(char* lewyOperand, char* prawyOperand, char* operacja,
	MapHash* zmienneProgramu, Stack* stosOperandow)
{
	int liczbaLewyOperand = 0;
	int liczbaPrawyOperand = 0;
	char* wynikWiekszyRowny = new char[MAX_DL_NAPISU];

	zarzadzajRodzajemOperandu(lewyOperand, prawyOperand, &liczbaLewyOperand,
		&liczbaPrawyOperand, zmienneProgramu, stosOperandow);

	if (czyKtorasStronaNul(stosOperandow, lewyOperand, prawyOperand))
		return;
	else if (liczbaLewyOperand >= liczbaPrawyOperand)
		stosOperandow->push(_strdup("0"));
	else
		stosOperandow->push(_strdup("#"));
	delete[] wynikWiekszyRowny;
}


void wykonajNegacje(char* operand, MapHash* zmienneProgramu, Stack* stosOperandow)
{
	int liczba = 0;
	char* liczbaString = new char[MAX_DL_NAPISU];

	if (czyLiczba(operand))
		stosOperandow->push(_strdup("#"));
	else if (!czyLiczba(operand))
	{
		if (!zmienneProgramu->getIsNull(operand))
		{
			liczba = -1 * zmienneProgramu->findElement(operand);
			stosOperandow->push(_strdup("#"));
		}
		else
		{
			zmienneProgramu->setIsNull(operand);
			stosOperandow->push(_strdup("0"));
			delete[] liczbaString;
			return;
		}
	}
	else if (strcmp(operand, "#") == 0)
	{
		stosOperandow->push(_strdup("0"));
		delete[] liczbaString;
		return;
	}

	delete[] liczbaString;
}


void znajdzRodzajOperacji(char* lewyOperand, char* prawyOperand, char* operacja,
	MapHash* zmienneProgramu, Stack* stosOperandow)
{
	if (strcmp(operacja, "=") == 0)
		wykonajPrzypisanieDoZmiennej(lewyOperand, prawyOperand, operacja, zmienneProgramu, stosOperandow);
	else if (strcmp(operacja, "+") == 0)
		wykonajDodawanie(lewyOperand, prawyOperand, operacja, zmienneProgramu, stosOperandow);
	else if (strcmp(operacja, "-") == 0)
		wykonajOdejmowanie(lewyOperand, prawyOperand, operacja, zmienneProgramu, stosOperandow);
	else if (strcmp(operacja, "*") == 0)
		wykonajMnozenie(lewyOperand, prawyOperand, operacja, zmienneProgramu, stosOperandow);
	else if (strcmp(operacja, "/") == 0)
		wykonajDzielenie(lewyOperand, prawyOperand, operacja, zmienneProgramu, stosOperandow);
	else if (strcmp(operacja, "%") == 0)
		wykonajModulo(lewyOperand, prawyOperand, operacja, zmienneProgramu, stosOperandow);
	else if (strcmp(operacja, "==") == 0)
		wykonajPorownanie(lewyOperand, prawyOperand, operacja, zmienneProgramu, stosOperandow);
	else if (strcmp(operacja, "!=") == 0)
		wykonajNierowny(lewyOperand, prawyOperand, operacja, zmienneProgramu, stosOperandow);
	else if (strcmp(operacja, ">") == 0)
		wykonajWiekszy(lewyOperand, prawyOperand, operacja, zmienneProgramu, stosOperandow);
	else if (strcmp(operacja, "<") == 0)
		wykonajMniejszy(lewyOperand, prawyOperand, operacja, zmienneProgramu, stosOperandow);
	else if (strcmp(operacja, ">=") == 0)
		wykonajWiekszyRowny(lewyOperand, prawyOperand, operacja, zmienneProgramu, stosOperandow);
	else if (strcmp(operacja, "<=") == 0)
		wykonajMniejszyRowny(lewyOperand, prawyOperand, operacja, zmienneProgramu, stosOperandow);
	else if (strcmp(operacja, "&") == 0)
		wykonajKoniunkcje(lewyOperand, prawyOperand, operacja, zmienneProgramu, stosOperandow);
	else if (strcmp(operacja, "|") == 0)
		wykonajKoniunkcje(lewyOperand, prawyOperand, operacja, zmienneProgramu, stosOperandow);
}

bool czyLiczycDalej(int* licznikOperacji, int maxOperacji)
{
	if (*licznikOperacji < maxOperacji)
		return true;
	return false;
}

void wykonajObliczeniaONP(Stack* operandy, Stack* operatoryZeStosu, MapHash* zmienneProgramu,
	int* licznikOperacji, int maxOperacji)
{
	if (czyLiczycDalej(licznikOperacji, maxOperacji))
	{
		///MUSZA BYC 2 OPERANDY ABY WYKONAC OBLICZENIA
		if (operandy->stackSize(operandy) >= 2 && operatoryZeStosu->stackSize(operatoryZeStosu) != 0)
		{
			char* operacja = _strdup(operatoryZeStosu->top());
			operatoryZeStosu->pop();
			if (strcmp(operacja, "!") == 0)
			{
				char* operand = _strdup(operandy->top());
				operandy->pop();

				if (*licznikOperacji < maxOperacji)
					(*licznikOperacji)++;

				wykonajNegacje(operand, zmienneProgramu, operatoryZeStosu);
				delete[] operand;
				return;
			}
			char* prawyOperand = _strdup(operandy->top());
			operandy->pop();
			char* lewyOperand = _strdup(operandy->top());
			operandy->pop();


			if (*licznikOperacji < maxOperacji)
				(*licznikOperacji)++;

			znajdzRodzajOperacji(lewyOperand, prawyOperand, operacja, zmienneProgramu, operandy);
		}
		else if (operandy->stackSize(operandy) == 1 && operatoryZeStosu->stackSize(operatoryZeStosu) != 0)
		{
			char* operacja = _strdup(operatoryZeStosu->top());
			operatoryZeStosu->pop();
			if (strcmp(operacja, "!") == 0)
			{
				char* operand = _strdup(operandy->top());
				operandy->pop();

				if (*licznikOperacji < maxOperacji)
					(*licznikOperacji)++;

				wykonajNegacje(operand, zmienneProgramu, operatoryZeStosu);
				delete[] operand;
				return;
			}
		}
	}
}

void zdejmijZeStosuPomiedzyNawiasami(Stack* stosOperatorow, Stack* stosOperandow, MapHash* zmienneProgramu,
	int* licznikOperacji, int maxOperacji)
{
	//stosOperatorow->pop();	//zdjecie znaku ")"
	while (strcmp(stosOperatorow->top(), "("))
	{
		wykonajObliczeniaONP(stosOperandow, stosOperatorow, zmienneProgramu, licznikOperacji, maxOperacji);
	}
	stosOperatorow->pop();
}


///SPRAWDZA PRIORYTETY I CZY POWINNA BYC NA STOSIE CZY PORA LICZYC ONP
void zbadajOperatoryDoKonwersji(char* znalezionyOperator, Stack* stosOperatorow,
	Stack* stosOperandow, MapHash* zmienneProgramu, int* licznikOperacji, int maxOperacji)
{
	int priorytet = znajdzPriorytet(znalezionyOperator);
	char* elementTop = new char[MAX_DL_NAPISU];
	if (czyLiczycDalej(licznikOperacji, maxOperacji))
	{
		if (stosOperatorow->top() == NULL)
		{
			stosOperatorow->push(znalezionyOperator);
			return;
		}


		while (stosOperatorow->next != NULL)
		{
			elementTop = _strdup(stosOperatorow->top());

			if (strcmp(elementTop, "(") == 0 || znajdzPriorytet(elementTop) < priorytet)
			{
				break;
			}

			//wykonuje i sciagam operatory o wyzszym lub rownym priorytecie 
			wykonajObliczeniaONP(stosOperandow, stosOperatorow, zmienneProgramu,
				licznikOperacji, maxOperacji);
		}
		stosOperatorow->push(znalezionyOperator);
	}
	delete[] elementTop;
}

void dokonczONP(Stack* stosOperatorow, Stack* stosOperandow, MapHash* zmienneProgramu,
	int* licznikOperacji, int maxOperacji)
{
	while (!stosOperatorow->isEmpty() && czyLiczycDalej(licznikOperacji, maxOperacji))
	{
		wykonajObliczeniaONP(stosOperandow, stosOperatorow, zmienneProgramu, licznikOperacji, maxOperacji);
	}
	if (stosOperatorow->isEmpty() && !stosOperandow->isEmpty())
	{
		char* operand = _strdup(stosOperandow->top());
		stosOperandow->pop();
		if (isalpha(operand[0]) && !zmienneProgramu->getIsNull(operand))
		{
			int liczba = zmienneProgramu->findElement(operand);
			char* liczbaString = new char[MAX_DL_NAPISU];
			itoa(liczba, liczbaString, 10);
			stosOperandow->push(liczbaString);
			delete[] liczbaString;

		}
		else if (czyLiczba(operand))
			stosOperandow->push(operand);

	}
	//czyscStos(stosOperandow);
	return;
}


//funkcja obliczajaca wartosc rownania
void wykonajONP(char* rownanie, MapHash* zmienneProgramu, Stack* stosOperandow,
	int* licznikOperacji, int maxOperacji)
{
	int i = 0;
	Stack stosOperatorow;
	int dlugoscRownania = strlen(rownanie);
	char* zmienna;
	char* liczba;
	char* znalezionyOperator;

	while (i < dlugoscRownania && czyLiczycDalej(licznikOperacji, maxOperacji))
	{
		//znalezienie liczby i dodanie jej na wyjscie
		if (isdigit(rownanie[i]))
		{
			//strcpy(liczba, znajdzLiczbe(rownanie, &i));
			liczba = _strdup(znajdzLiczbe(rownanie, &i));
			stosOperandow->push(liczba);
		}
		//znalezienie liczby ujemnej o ile pierwszy znak jest '-' po operatorze
		else if (czyOperator(rownanie[i]) && pokazNajblizszyZnak(rownanie, i) == '-')
		{
			//znalezienie operatora
			//strcpy(znalezionyOperator, znajdzOperator(rownanie, &i));
			znalezionyOperator = _strdup(znajdzOperator(rownanie, &i));
			zbadajOperatoryDoKonwersji(znalezionyOperator, &stosOperatorow, stosOperandow,
				zmienneProgramu, licznikOperacji, maxOperacji);

			//znalezienie liczby ujemnej
			//strcpy(liczba, znajdzLiczbe(rownanie, &i));
			liczba = _strdup(znajdzLiczbe(rownanie, &i));
			stosOperandow->push(liczba);
			continue;
		}
		//znalezienie wyrazu (zmiennej jakiej� juz znanej i dodanej wczesniej
		//do tablicy hashowanej) i dodanie jej na wyjscie
		else if (isalpha(rownanie[i]))
		{
			//strcpy(zmienna, znajdzWyraz(rownanie, &i));
			zmienna = _strdup(znajdzWyraz(rownanie, &i));
			stosOperandow->push(zmienna);
		}
		//znalezienie operatora i dodanie go na stos oraz dodanie na wyjscie operatorow o wyzszym priorytecie 
		else if (czyOperator(rownanie[i]) && pokazNajblizszyZnak(rownanie, i) != '-')
		{
			//strcpy(znalezionyOperator, znajdzOperator(rownanie, &i));
			znalezionyOperator = _strdup(znajdzOperator(rownanie, &i));
			zbadajOperatoryDoKonwersji(znalezionyOperator, &stosOperatorow, stosOperandow, zmienneProgramu,
				licznikOperacji, maxOperacji);
		}
		else if (rownanie[i] == '(')
		{
			char* nawias = _strdup("(");
			stosOperatorow.push(nawias);
			i++;
		}
		else if (rownanie[i] == ')')
		{
			zdejmijZeStosuPomiedzyNawiasami(&stosOperatorow, stosOperandow, zmienneProgramu,
				licznikOperacji, maxOperacji);
			i++;
		}
		else
			i++;
	}
	if (czyLiczycDalej(licznikOperacji, maxOperacji))
		dokonczONP(&stosOperatorow, stosOperandow, zmienneProgramu, licznikOperacji, maxOperacji);
	/*delete[] znalezionyOperator;
	delete[] zmienna;
	delete[] liczba;*/
}

int przesunIndex(char * wczytanyKod, int indexOrginalny)
{
	int indexPoPetli = 0;
	int nastepnyIndex = indexOrginalny;
	while (wczytanyKod[nastepnyIndex] != '{')
		(nastepnyIndex)++;
	int zaglebienieKlamr = 1;
	nastepnyIndex++;
	while (zaglebienieKlamr > 0 && wczytanyKod[nastepnyIndex + 1] != '\0')
	{
		if (wczytanyKod[nastepnyIndex] == '{')
		{
			zaglebienieKlamr++;
		}
		else if (wczytanyKod[nastepnyIndex] == '}')
		{
			zaglebienieKlamr--;
		}
		(nastepnyIndex)++;
	}
	//nastepnyIndex++;
	indexPoPetli = nastepnyIndex;
	return indexPoPetli;
}

// szukanie warunku miedzy nawiasami (kod miedzy nawiasami)
char* znajdzWarunek(char* wczytanyKod, int* kolumnaInstrukcji, MapHash* zmienneProgramu, int* licznikOperacji,
	int maxOperacji)
{
	int maxDlNapisu = MAX_DL_NAPISU;
	int indexWarunku = 0;
	int dlugoscLinii = strlen(wczytanyKod);
	char znalezionyWarunek[MAX_DL_NAPISU];

	(*kolumnaInstrukcji) = (*kolumnaInstrukcji) + 2;	//bo nie wczytuje znaku '@' ani '('

	while (*kolumnaInstrukcji < dlugoscLinii && czyLiczycDalej(licznikOperacji, maxOperacji) &&
		indexWarunku < dlugoscLinii)
	{
		if (wczytanyKod[*kolumnaInstrukcji] == ')' && pokazNajblizszyZnak(wczytanyKod, *kolumnaInstrukcji) == '{')
		{
			znalezionyWarunek[indexWarunku] = '\0';
			return znalezionyWarunek;
		}
		else if (wczytanyKod[*kolumnaInstrukcji] == ' ')
		{
			(*kolumnaInstrukcji)++;
		}
		else
		{
			znalezionyWarunek[indexWarunku] = wczytanyKod[*kolumnaInstrukcji];
			indexWarunku++;
			(*kolumnaInstrukcji)++;
		}
	}
	return znalezionyWarunek;
}

//warunek do petli miedzy nawiasami
bool liczWarunek(char* kodWarunku, MapHash* zmienneProgramu, int* licznikOperacji, int maxOperacji)
{
	if (*licznikOperacji < maxOperacji)
		(*licznikOperacji)++;
	Stack stosOperandow;
	if (czyLiczycDalej(licznikOperacji, maxOperacji))
		wykonajONP(kodWarunku, zmienneProgramu, &stosOperandow, licznikOperacji, maxOperacji);
	else
		return false;

	if (!stosOperandow.isEmpty() && strcmp(stosOperandow.top(), "#"))
	{
		czyscStos(&stosOperandow);
		return true;
	}
	else if (!czyLiczycDalej(licznikOperacji, maxOperacji))
	{
		czyscStos(&stosOperandow);
		return false;
	}
	czyscStos(&stosOperandow);
	return false;
}


//w przypadku true wywoluje ONP
//jesli false nic nie robi
bool czyPrzekazacDoONP(char* wczytanyKod, MapHash* zmienneProgramu, Stack* stosOperandow,
	char* rownanie, int* kolumna, int* dlugoscKodu, int* licznikOperacji, int maxOperacji)
{
	char znak = pokazNajblizszyZnak(wczytanyKod, *kolumna);
	if (isalnum(znak) || znak == '?' || znak == '@' || znak == '\0')
	{
		rownanie[*dlugoscKodu] = '\0';
		wykonajONP(rownanie, zmienneProgramu, stosOperandow, licznikOperacji, maxOperacji);
		rownanie[*dlugoscKodu] = wczytanyKod[*kolumna];
		*dlugoscKodu = 0;
		return true;
	}
	return false;
}

//jesli true to znaleziono rownanie i wykonano ONP
//jesli false to nic nie zrobiono
void sprawdzKoniecInstrukcji(char* wczytanyKod, char* rownanie, int* kolumnaKodu,
	int* dlugoscRownania, MapHash* zmienneProgramu, Stack* stosOperandow, int* licznikOperacji, int maxOperacji)
{

	if (isalnum(wczytanyKod[*kolumnaKodu]) && wczytanyKod[(*kolumnaKodu) + 1] == ' ')
	{
		rownanie[*dlugoscRownania] = wczytanyKod[*kolumnaKodu];
		(*dlugoscRownania)++;
		(*kolumnaKodu)++;
		if (czyPrzekazacDoONP(wczytanyKod, zmienneProgramu, stosOperandow,
			rownanie, kolumnaKodu, dlugoscRownania, licznikOperacji, maxOperacji))
			return;
	}

	else if (wczytanyKod[*kolumnaKodu] == ')' && wczytanyKod[(*kolumnaKodu) + 1] == ' ')
	{
		rownanie[*dlugoscRownania] = wczytanyKod[*kolumnaKodu];
		(*dlugoscRownania)++;
		(*kolumnaKodu)++;
		if (czyPrzekazacDoONP(wczytanyKod, zmienneProgramu, stosOperandow,
			rownanie, kolumnaKodu, dlugoscRownania, licznikOperacji, maxOperacji))
			return;
	}
	else if (wczytanyKod[*kolumnaKodu] == '}' && wczytanyKod[(*kolumnaKodu) + 1] == ' ')
	{
		if (czyPrzekazacDoONP(wczytanyKod, zmienneProgramu, stosOperandow,
			rownanie, kolumnaKodu, dlugoscRownania, licznikOperacji, maxOperacji))
			return;
	}
	else if (isalnum(wczytanyKod[(*kolumnaKodu)]) && wczytanyKod[(*kolumnaKodu) + 1] == '\0')
	{
		rownanie[*dlugoscRownania] = wczytanyKod[*kolumnaKodu];
		(*dlugoscRownania)++;
		(*kolumnaKodu)++;
		rownanie[*dlugoscRownania] = '\0';
		wykonajONP(rownanie, zmienneProgramu, stosOperandow, licznikOperacji, maxOperacji);
		*dlugoscRownania = 0;
		return;
	}

	return;
}

void wykonajIf(char* wczytanyKod, int* kolumnaKodu, MapHash* zmienneProgramu,
	Stack* stosOperandow, int* licznikOperacji, int maxOperacji)
{
	int tmpKolumnyIndex = *kolumnaKodu;
	int dlugoscKodu = strlen(wczytanyKod);
	char warunek[MAX_DL_NAPISU];
	strcpy(warunek, znajdzWarunek(wczytanyKod, &tmpKolumnyIndex, zmienneProgramu, licznikOperacji, maxOperacji));

	int indexKoncaWarunku = tmpKolumnyIndex;
	int maxDlNapisu = MAX_DL_NAPISU;
	int indexInstrukcji = 0;

	if (!czyLiczycDalej(licznikOperacji, maxOperacji))
		return;
	else if (liczWarunek(warunek, zmienneProgramu, licznikOperacji, maxOperacji))
	{
		if (czyLiczycDalej(licznikOperacji, maxOperacji))
		{
			indexKoncaWarunku = tmpKolumnyIndex;
			wykonujPomiedzyKlamrami(wczytanyKod, &indexKoncaWarunku, zmienneProgramu,
				&indexInstrukcji, stosOperandow, licznikOperacji, maxOperacji);
			indexKoncaWarunku = tmpKolumnyIndex;
		}
	}

	indexKoncaWarunku = tmpKolumnyIndex;
	*kolumnaKodu = indexKoncaWarunku;
	indexInstrukcji = 0;
	//free(warunek);
}

void wykonajWhile(char * wczytanyKod, int * kolumnaKodu, MapHash * zmienneProgramu,
	Stack* stosOperandow, int* licznikOperacji, int maxOperacji)
{
	int tmpKolumnyIndex = *kolumnaKodu;
	char warunek[MAX_DL_NAPISU];
	strcpy(warunek, znajdzWarunek(wczytanyKod, &tmpKolumnyIndex, zmienneProgramu, licznikOperacji, maxOperacji));
	//tmpKolumnyIndex = *kolumnaKodu;

	int indexInstrukcji = 0;
	int indexKoncaWarunku = tmpKolumnyIndex;

	if (!czyLiczycDalej(licznikOperacji, maxOperacji))
		return;
	while (liczWarunek(warunek, zmienneProgramu, licznikOperacji, maxOperacji))
	{
		if (czyLiczycDalej(licznikOperacji, maxOperacji))
		{
			indexKoncaWarunku = tmpKolumnyIndex;
			wykonujPomiedzyKlamrami(wczytanyKod, &indexKoncaWarunku, zmienneProgramu, &indexInstrukcji, stosOperandow, licznikOperacji, maxOperacji);
			indexInstrukcji = 0;
			indexKoncaWarunku = tmpKolumnyIndex;
		}
		else
			break;
	}
	indexKoncaWarunku = tmpKolumnyIndex;
	*kolumnaKodu = indexKoncaWarunku;
	indexInstrukcji = 0;

}

//funkcja ktora przechodzi po calym kodzie, szukajaca rownania do ONP
void szukajRowaniaDoONP(char* wczytanyKod, MapHash* zmienneProgramu, int* licznikOperacji, int maxOperacji)
{
	int i = 0;
	int dlugoscKodu = strlen(wczytanyKod);
	Stack stosOperandow;
	int indexDoObliczen = 0;
	int maxDlugoscRownania = MAX_DL_NAPISU;
	int dlugoscRownania = 0;
	char* rownanie = new char[MAX_DL_NAPISU];
	while (i < dlugoscKodu && czyLiczycDalej(licznikOperacji, maxOperacji))
	{
		indexDoObliczen = i;
		if (dlugoscRownania == maxDlugoscRownania - 1)
		{
			maxDlugoscRownania *= maxDlugoscRownania;
			realloc(rownanie, maxDlugoscRownania*sizeof(char));
		}

		sprawdzKoniecInstrukcji(wczytanyKod, rownanie, &i, &dlugoscRownania, zmienneProgramu,
			&stosOperandow, licznikOperacji, maxOperacji);

		if (wczytanyKod[i] == '?')
		{
			wykonajIf(wczytanyKod, &i, zmienneProgramu, &stosOperandow, licznikOperacji, maxOperacji);

			i = przesunIndex(wczytanyKod, i);
			/*if (i == dlugoscKodu)
				i--;*/
		}
		else if (wczytanyKod[i] == '@')
		{
			int wystapienieWhile = i;
			wykonajWhile(wczytanyKod, &i, zmienneProgramu, &stosOperandow, licznikOperacji, maxOperacji);

			i = przesunIndex(wczytanyKod, i);
			/*if (i == dlugoscKodu)
				i--;*/
		}
		else if (wczytanyKod[i] == ' ')
			i++;
		else
		{
			rownanie[dlugoscRownania] = wczytanyKod[i];
			dlugoscRownania++;
			i++;
		}

	}
	delete[] rownanie;
}

void wykonujPomiedzyKlamrami(char* wczytanyKod, int* kolumnaKodu, MapHash* zmienneProgramu,
	int* indexInstrukcji, Stack* stosOperandow, int* licznikOperacji, int maxOperacji)
{
	int tmpIloscOperacji = *licznikOperacji;
	bool poczatekKonca = false;
	(*kolumnaKodu) += 3;
	int tmpIndexKodu = *kolumnaKodu;
	int dlugoscWczytanegoKodu = strlen(wczytanyKod);
	char* instrukcjeTmp = new char[MAX_DL_NAPISU];
	while (wczytanyKod[tmpIndexKodu] != '}')
	{
		if (wczytanyKod[tmpIndexKodu] == '?')
		{
			wykonajIf(wczytanyKod, &tmpIndexKodu, zmienneProgramu, stosOperandow,
				licznikOperacji, maxOperacji);
			tmpIndexKodu = przesunIndex(wczytanyKod, tmpIndexKodu);
			continue;
		}
		else if (wczytanyKod[tmpIndexKodu] == '@')
		{
			int testPomiedzyKlamrami = tmpIndexKodu;
			wykonajWhile(wczytanyKod, &tmpIndexKodu, zmienneProgramu, stosOperandow,
				licznikOperacji, maxOperacji);
			tmpIndexKodu = przesunIndex(wczytanyKod, tmpIndexKodu);
			continue;
			
			//printf("%c", wczytanyKod[*kolumnaKodu]);
		}
		else if (wczytanyKod[tmpIndexKodu] == ' ' || wczytanyKod[tmpIndexKodu] == '\t')
		{
			(tmpIndexKodu)++;
			continue;
		}

		else
		{
			instrukcjeTmp[*indexInstrukcji] = wczytanyKod[tmpIndexKodu];
			if (isalnum(wczytanyKod[tmpIndexKodu]) || wczytanyKod[tmpIndexKodu] == ')')
				poczatekKonca = true;
			else
				poczatekKonca = false;
			(*indexInstrukcji)++;
			(tmpIndexKodu)++;
		}

		char najblizszyZnak = pokazNajblizszyZnak(wczytanyKod, tmpIndexKodu);

		if (wczytanyKod[tmpIndexKodu] == ' ' && poczatekKonca &&
			(isalnum(najblizszyZnak) || najblizszyZnak == '(' || najblizszyZnak == '{' ||
				najblizszyZnak == '\0' || najblizszyZnak == '}' || najblizszyZnak == '?' || najblizszyZnak == '@'))
		{
			while (wczytanyKod[tmpIndexKodu] != najblizszyZnak)
			{
				(tmpIndexKodu)++;
			}
			instrukcjeTmp[*indexInstrukcji] = '\0';
			wykonajONP(instrukcjeTmp, zmienneProgramu, stosOperandow, licznikOperacji, maxOperacji);
			czyscStos(stosOperandow);
			instrukcjeTmp[*indexInstrukcji] = wczytanyKod[tmpIndexKodu];
			*indexInstrukcji = 0;
			poczatekKonca = false;
		}
		else if (wczytanyKod[(tmpIndexKodu) + 1] == '}')
		{
			instrukcjeTmp[*indexInstrukcji] = wczytanyKod[tmpIndexKodu];
			(*indexInstrukcji)++;
			instrukcjeTmp[*indexInstrukcji] = '\0';
			wykonajONP(instrukcjeTmp, zmienneProgramu, stosOperandow, licznikOperacji, maxOperacji);
			czyscStos(stosOperandow);
			instrukcjeTmp[*indexInstrukcji] = wczytanyKod[tmpIndexKodu];
			*indexInstrukcji = 0;
			poczatekKonca = false;
		}


		///wejdzie tutaj tylko wtedy gdy

	}
	*indexInstrukcji = 0;
	//delete[] instrukcjeTmp;
}

int skryptMario()
{
	unsigned int maxLiczbaKodu = MAX_LICZBA_KODU;
	unsigned int maxLiczbaZmiennych = MAX_LICZBA_ZMIENNYCH;
	MapHash zmienneProgramuHash;
	Stack stosOperatorow;
	unsigned long long int iloscOperacji = 0;
	unsigned long long int licznikZmiennychDoWyswietlenia = 0;
	char** tablicaZmiennychDoWyswietlenia = new char*[MAX_LICZBA_ZMIENNYCH];	//tylko do trzymania nazw zmiennych
	char** linieKodu = new char*[MAX_LICZBA_KODU];
	char wczytanaLinia[MAX_DL_LINII];
	char* tmpWczytanaLinia = new char[MAX_DL_LINII];
	wypelnijTabliceOperatorow();

	char** zmienneJednejLinii = new char*[MAX_DL_LINII];
	char** wszystkieZmienneWProgramie = new char*[MAX_LICZBA_ZMIENNYCH];

	scanf("%llu", &iloscOperacji);

	//wczytywanie nazw zmiennych
	scanf(" %1000[^\n]", &wczytanaLinia);
	//przeniesienie do tempa aby moc potem to skopiowac do wlasciwej tablicy
	tmpWczytanaLinia = strtok(wczytanaLinia, " ");

	for (int i = 0; i < MAX_LICZBA_ZMIENNYCH; i++)
	{
		tablicaZmiennychDoWyswietlenia[i] = new char[MAX_DL_NAPISU];
		linieKodu[i] = new char[MAX_DL_NAPISU];
		wszystkieZmienneWProgramie[i] = new char[MAX_DL_NAPISU];
		zmienneJednejLinii[i] = new char[MAX_DL_NAPISU];
	}

	//ciecie tekstu i przypisanie go do tablicy zmiennych
	while (tmpWczytanaLinia != NULL)
	{
		strcpy(tablicaZmiennychDoWyswietlenia[licznikZmiennychDoWyswietlenia], tmpWczytanaLinia);
		tmpWczytanaLinia = strtok(NULL, " ");
		licznikZmiennychDoWyswietlenia++;
	}
	delete[] tmpWczytanaLinia;

	//hashowanie zmiennych z pierwszej linii
	for (int i = 0; i < licznikZmiennychDoWyswietlenia; i++)
	{
		zmienneProgramuHash.addElement(0, tablicaZmiennychDoWyswietlenia[i]);
	}

	///-------------------------------------------WCZYTYWANIE LINIJEK KODU (LINIJKA PO LINIJCE)------------------------
	int licznikLiniiKodu = 0;
	while (fgets(linieKodu[licznikLiniiKodu], MAX_LICZBA_KODU, stdin))
	{
		if (licznikLiniiKodu == maxLiczbaKodu - 1)
		{
			maxLiczbaKodu *= 2;
			maxLiczbaZmiennych *= 2;
			realloc(linieKodu, maxLiczbaKodu*sizeof(char));
			realloc(wszystkieZmienneWProgramie, maxLiczbaZmiennych*sizeof(char));
			realloc(zmienneJednejLinii, maxLiczbaZmiennych*sizeof(char));
		}
		usunKoniecLinii(linieKodu[licznikLiniiKodu]);
		++licznikLiniiKodu;
	}

	//wyswietlKod(linieKodu, licznikLiniiKodu);	//do testu


	///====================================SZUKANIE NOWYCH ZMIENNYCH=====================================================


	//KOPIOWANIE DO NOWEJ TABLICY ZMIENNYCH Z DRUGIEJ LINII
	//NOWA TABLICA Z WSZYSTKIMI ZMIENNYMI W PROGRAMIE
	int licznikZmiennychWProgramie = licznikZmiennychDoWyswietlenia;
	for (int j = 0; j < licznikZmiennychWProgramie; j++)
	{
		strcpy(wszystkieZmienneWProgramie[j], tablicaZmiennychDoWyswietlenia[j]);
		//printf("%s\n", wszystkieZmienneWProgramie[j]);
	}


	for (int i = 0; i < licznikLiniiKodu; i++)
	{
		int iloscWyrazowJendejLinii = 0;

		zmienneJednejLinii = szukajWyrazow(linieKodu[i], &iloscWyrazowJendejLinii);

		//wyswietlKod(wyrazyJednejLinii, iloscWyrazowJendejLinii);	//test


		//-----------------ZNALEZIENIE NOWYCH ZMIENNYCH + HASHOWANIE ICH   (z wyrazyJednejLinii)---------------------------
		for (int j = 0; j < iloscWyrazowJendejLinii; j++)
		{
			if (czyUnikatowaZmienna(zmienneJednejLinii[j], wszystkieZmienneWProgramie, licznikZmiennychWProgramie))
			{
				//strcpy(wszystkieZmienneWProgramie[licznikZmiennychWProgramie], zmienneJednejLinii[j]);
				wszystkieZmienneWProgramie[licznikZmiennychWProgramie] = _strdup(zmienneJednejLinii[j]);
				zmienneProgramuHash.addElement(0, wszystkieZmienneWProgramie[licznikZmiennychWProgramie]);
				++licznikZmiennychWProgramie;
			}
		}
	}

	/*for (int i = 0; i < licznikZmiennychWProgramie-1; i++)
	{
		printf("%s\n", zmienneJednejLinii[i]);
	}*/

	char* sformatowanyKod;

	sformatowanyKod = _strdup(formatujKod(linieKodu, licznikLiniiKodu));
	//printf("%s\n", sformatowanyKod);


	int zliczoneOperacje = 0;
	szukajRowaniaDoONP(sformatowanyKod, &zmienneProgramuHash, &zliczoneOperacje, iloscOperacji);

	printf("%d\n", zliczoneOperacje);
	for (int i = 0; i < licznikZmiennychDoWyswietlenia; i++)
	{
		if (zmienneProgramuHash.getIsNull(wszystkieZmienneWProgramie[i]))
			printf("%s Nul\n", tablicaZmiennychDoWyswietlenia[i]);
		else
			printf("%s %d\n", wszystkieZmienneWProgramie[i],
				zmienneProgramuHash.findElement(tablicaZmiennychDoWyswietlenia[i]));
	}


	///---------------------------------ZWALNIANIE PAMIECI-------------------------------

	delete[] sformatowanyKod;

	//usuwanie zmiennych z jednej linii
	for (int i = 0; i < MAX_DL_LINII; i++)
	{
		delete[] zmienneJednejLinii[i];
	}

	for (int i = 0; i < MAX_LICZBA_ZMIENNYCH; i++)
	{
		delete[] tablicaZmiennychDoWyswietlenia[i];
		delete[] wszystkieZmienneWProgramie[i];

	}
	delete[] tablicaZmiennychDoWyswietlenia;
	delete[] wszystkieZmienneWProgramie;

	for (int i = 0; i < MAX_LICZBA_KODU; i++)
	{
		delete[] linieKodu[i];
	}
	delete[] linieKodu;

	return 0;
}





int main()
{
	skryptMario();

	//testRozmiaruTablicy();

	return 0;
}

