#pragma once
class Pair
{
public:

	char* someString;
	int value;
	bool isNull;

	Pair(const Pair& right);
	Pair(char* string, int value);
	void setValue(int value);
	void setisNull(bool isNull);
	~Pair();
};

